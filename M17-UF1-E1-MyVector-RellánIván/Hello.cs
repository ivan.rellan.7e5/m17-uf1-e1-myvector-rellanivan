﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_RellánIván
{
    class Hello
    {
        public void YourName()
        {
            Console.Write("What's your name?: ");
            string name;

            do
            {
                name = Console.ReadLine();
            } while (name == null);

            Console.WriteLine("Hello, {0}", name);
        }
    }
}
