﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Xml;

namespace M17_UF1_E1_MyVector_RellánIván
{
    class MyVector
    {
        private double[,] vector = new double[2, 2];
        //private double[] vectorComponents = new double[2];

        public MyVector(double[,] vector)
        {
            this.vector = vector;
        }

        public double[,] GetVector()
        {
            return vector;
        }

        public void SetVector(double[,] vector)
        {
            for(var i = 0; i < this.vector.GetLength(1); i++)
            {
                for (var j = 0; j < this.vector.GetLength(0); j++)
                {
                    this.vector[j, i] = vector[j, i];
                };
            };
        }

        public double VectorDistance()
        {

            return Math.Sqrt(Math.Pow(vector[1, 0] - vector[0, 0], 2) + Math.Pow(vector[1, 1] - vector[0, 1], 2));
        }

        public double VectorDistanceToOrigin()
        {
            var n1 = Math.Sqrt(Math.Pow(vector[0, 0] - 0, 2) + Math.Pow(vector[0, 1] - 0, 2));
            var n2 = Math.Sqrt(Math.Pow(vector[1, 0] - 0, 2) + Math.Pow(vector[1, 1] - 0, 2));

            double result = n1;

            if (n2 < n1) result = n2;
            return result;
        }

        public void InvertVector()
        {
            double[] inici = { vector[0, 0], vector[0, 1] };
            vector[0, 0] = vector[1, 0];
            vector[0, 1] = vector[1, 1];
            vector[1, 0] = inici[0];
            vector[1, 1] = inici[1];
        }

        public override string ToString()
        {
            double a = vector[1, 0] - vector[0, 0];
            double b = vector[1, 1] - vector[0, 1];
            return "MyVector info: AB = " + a + "i " + b + "j. INICI: X1 = " + vector[0, 0] + ", Y1 = " + vector[0, 1] + ". FINAL: X2 = " + vector[1, 0] + ", Y2 = " + vector[1, 1] + ". DISTANCIA: " + this.VectorDistance() + ". DISTANCIA MÍNIMA AL ORIGEN: " + VectorDistanceToOrigin();
        }

        public int CompareTo([AllowNull] MyVector other, bool opcio)
        {
            int ordre = 0;
            switch (opcio)
            {
                case true:
                    if (VectorDistance() <= other.VectorDistance()) ordre = 1;
                    break;
                case false:
                    ordre = 1;
                    if (VectorDistanceToOrigin() < other.VectorDistanceToOrigin()) ordre = 0;
                    break;
            }

            return ordre;
        }
    }
}
