﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Printing;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Text.RegularExpressions;

namespace M17_UF1_E1_MyVector_RellánIván
{
    class VectorGame
    {
        public MyVector[] RandomVectors(int arrLong)
        {
            var ran = new Random();
            var ranArr = new MyVector[arrLong];
            for (var i = 0; i < arrLong; i++)
            {
                var newVector = new double[2, 2] { { ran.Next(-11, 10), ran.Next(-11, 10) }, { ran.Next(-11, 10), ran.Next(-11, 10) } };
                ranArr[i] = new MyVector(newVector);
            }

            return ranArr;
        }

        public void SortVectors(MyVector[] vectors, bool option)
        {
            bool finished;
            do
            {
                finished = true;
                for (int i = 0, j = 1; i < vectors.Length - 1 && j < vectors.Length; i++, j++)
                {
                    if (vectors[j].CompareTo(vectors[i], option) == 0)
                    {
                        (vectors[j], vectors[i]) = (vectors[i], vectors[j]);
                        finished = false;
                    }
                }
            } while (!finished);
        }

        public void ShowVector(MyVector vector/*, PaintEventArgs paint*/)
        {
            Console.Clear();
            var tan = (vector.GetVector()[1, 0] - vector.GetVector()[0, 0]) / (vector.GetVector()[1, 1] - vector.GetVector()[0, 1]);
            var newOrigin = new int[] {Console.WindowWidth / 2, Console.WindowHeight / 2};

            //Console.SetCursorPosition((int)Math.Round(vector.GetVector()[0, 0]) + newOrigin[0], (int)Math.Round(vector.GetVector()[0, 1]) + newOrigin[0]);

            var v = new[] { vector.GetVector()[0, 1] - vector.GetVector()[0, 0], vector.GetVector()[1, 1] - vector.GetVector()[1, 0] };
            var vMod = Math.Sqrt(Math.Pow(v[0], 2) + Math.Pow(v[1], 2));
            var n = new[] { v[0] / vMod, v[1] / vMod };

            for (int i = 0, j = 1; i < 20; i++, j++)
            {
                var c = "#";
                if (i == 0) c = "0";

                var x = (int)Math.Round(n[0] * j) + newOrigin[0];
                var y = -(int)Math.Round(n[1] * j) + newOrigin[1];

                if (x < 0 || y < 0 || x > Console.WindowWidth || y > Console.WindowHeight) break;

                Console.SetCursorPosition(x, y);
                Console.WriteLine(c);
            }

            Console.SetCursorPosition(0, Console.WindowHeight);

            //(int)Math.Round(vector.GetVector()[0, 0]), (int)Math.Round(vector.GetVector()[0, 1])

            /*var a = Console.CursorLeft;
            while (a < Console.WindowWidth)
            {
                
                Console.SetCursorPosition(a, Console.WindowHeight / 2);
                Console.WriteLine("_");
                a++;
            }

            while (Console.CursorTop < Console.WindowHeight)
            {
                Console.SetCursorPosition(Console.WindowWidth / 2, Console.CursorTop);
                Console.WriteLine("|");
                Console.CursorTop++;
            }*/

            /*Pen white = new Pen(Color.White, 1);

            paint.Graphics.DrawLine(white, (int)Math.Round(vector.GetVector()[0, 0]), (int)Math.Round(vector.GetVector()[0, 1]), (int)Math.Round(vector.GetVector()[1, 0]), (int)Math.Round(vector.GetVector()[1, 1]));*/
        }
    }
}
