﻿using System;

namespace M17_UF1_E1_MyVector_RellánIván
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("PRESENTACIÓN: HELLO {name}");
            new Hello().YourName();
            System.Threading.Thread.Sleep(1000);

            Console.WriteLine("\nCLASE MYVECTOR: ");
            var vector = new MyVector(new double[,] { { 8.6, 3.7 }, { 10.9, -50.5 } });
            Console.WriteLine("VECTOR 1: " + vector);
            var vector2 = vector.GetVector();
            Console.WriteLine("VECTOR 2 (no objeto MyVector): " + vector2);
            vector.SetVector(new double[,] { { 19, 30.3 }, { 10.8, -10 } });
            Console.WriteLine("VECTOR 1 MODIFICADO: " + vector);
            Console.WriteLine("DISTANCIA VECTOR 1: " + vector.VectorDistance());
            vector.InvertVector();
            Console.WriteLine("VECTOR 1 INVERTIDO: " + vector + "\n");
            /*Console.WriteLine("Presiona cualquier tecla para continuar...");
            Console.Read();*/

            Console.WriteLine("\nCLASE VECTOR GAME: ");
            var vectorGame = new VectorGame();
            var ranVectors = vectorGame.RandomVectors(6);
            Console.WriteLine("VECTORES ALEATORIOS: ");
            for (var i = 0; i < ranVectors.Length; i++)
            {
                Console.WriteLine(ranVectors[i] + "\n");
            }
            System.Threading.Thread.Sleep(1000);

            vectorGame.SortVectors(ranVectors, true);
            Console.WriteLine("VECTORES ORDENADOS POR DISTANCIA (de mayor a menor): ");
            for (var i = 0; i < ranVectors.Length; i++)
            {
                Console.WriteLine(ranVectors[i] + "\n");
            }
            System.Threading.Thread.Sleep(1000);

            vectorGame.SortVectors(ranVectors, false);
            Console.WriteLine("VECTORES ORDENADOS POR DISTANCIA AL ORIGEN (del más cercano al más lejano): ");
            for (var i = 0; i < ranVectors.Length; i++)
            {
                Console.WriteLine(ranVectors[i] + "\n");
            }
            System.Threading.Thread.Sleep(1000);


            Console.WriteLine("Presiona cualquier tecla para continuar...");
            Console.Read();


            vectorGame.ShowVector(ranVectors[0]);
            Console.WriteLine("REPRESENTACIÓN GRÁFICA (donde 0 es el punto de origen) DE LA DIRECCIÓN DE: " + ranVectors[0]);
            Console.WriteLine("Presiona cualquier tecla para continuar...");
            Console.ReadKey();

            vectorGame.ShowVector(ranVectors[1]);
            Console.WriteLine("REPRESENTACIÓN GRÁFICA (donde 0 es el punto de origen) DE LA DIRECCIÓN DE: " + ranVectors[1]);
            Console.WriteLine("Presiona cualquier tecla para continuar...");
            Console.ReadKey();

            vectorGame.ShowVector(ranVectors[3]);
            Console.WriteLine("REPRESENTACIÓN GRÁFICA (donde 0 es el punto de origen) DE LA DIRECCIÓN DE: " + ranVectors[3]);
        }
    }
}
